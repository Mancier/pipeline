Algumas funções que podem te apoiar

=> malloc()\
    Aloca em memória algum dado e retorna o endreço desse dado. POINTER
    `int* malloc(<variavel>`
    
=> sizeof()\
    Retorna o tamanho em bytes daquela função ou variavel especifica.
    `int sizeof(<* variavel>)`
    
=> &\
    Normalmente seu usa isso para pegar o endereço de memória daquela variável
    `$variavel`

